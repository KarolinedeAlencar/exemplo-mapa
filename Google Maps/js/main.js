function initMap() {

  var markers = [], marker, i;
  var infowindow = new google.maps.InfoWindow();

  // Dados
  var locations = [

    {
      'titulo': 'Bondi Beach',
      'descricao': 'Uma praia bonita. 1',
      'lat': -33.890542,
      'lng': 151.274856
    },

    {
      'titulo': 'Coogee Beach',
      'descricao': 'Uma praia bonita. 2',
      'lat': -33.923036,
      'lng': 151.259052
    },

    {
      'titulo': 'Cronulla Beach',
      'descricao': 'Uma praia bonita. 3',
      'lat': -34.028249,
      'lng': 151.157507
    },

    {
      'titulo': 'Manly Beach',
      'descricao': 'Uma praia bonita. 4',
      'lat': -33.80010128657071,
      'lng': 151.28747820854187
    },

    {
      'titulo': 'Maroubra Beach',
      'descricao': 'Uma praia bonita.5',
      'lat': -33.950198,
      'lng':  151.259302
    }

  ];


  // Opções e estilos do Cluster
  var clusterOptions = {

    styles: [
      {
        url: 'img/cluster-pin.png',
        height: 50,
        width: 50,
        anchor: [8, -1],
        textSize: 18
      }
    ],

    zoomOnClick: false
  }

  // Renderizando o mapa
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 10,
    center: new google.maps.LatLng(-33.92, 151.25),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });


  // Loop pelos dados
  for (i = 0; i < locations.length; i++) {
    var infowindow = new google.maps.InfoWindow();

    // Criando um marcador com as informações
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
      map: map,
      icon: 'img/pin.png',
      titulo: locations[i].titulo,
      descricao: locations[i].descricao
    });

    // Inserindo o marcador em um array para abrir no Cluster depois
    markers.push(marker);

    // Escutando click no marcador
    marker.addListener('click', function() {
			abrirInfoWindow(this, infowindow);
		});

  }


  // Criando o Cluster (Grupo) e passando o mapa, array de marcadores e as opções/estilos
  var markerCluster = new MarkerClusterer(map, markers, clusterOptions);

  // Escutando click no Cluster
  google.maps.event.addListener(markerCluster, 'clusterclick', abrirInformacoesCluster);

  function abrirInfoWindow(marker, infowindow) {

    // Definindo o conteúdo da infowindow (A janela do conteúdo)
    infowindow.setContent(marker.descricao);

    // Abrindo a InfoWindow
    infowindow.open(map, marker);

  }


  function abrirInformacoesCluster(cluster) {

    // Pegando os marcadores dentro do Cluster
    var clustersMarkers = cluster.getMarkers();

    var arrClusters = [];
    var containerString, newArrClusters;


    // Loop pelos marcadores do Clusters
    for (i = 0; i < clustersMarkers.length; i ++) {

      // Marcação HTML com os dados dos marcadores
      arrClusters.push('<div class="praia">' +

        '<h2 class="praia__titulo">' + clustersMarkers[i].titulo + '</h2>' +
        '<p class="praiar__desc">' + clustersMarkers[i].descricao + '</p>' +

        '</div>'
      );
    }

    // Dando Join no array (Join junta os elementos do array dentro de uma STRING)
    newArrClusters =  arrClusters.join('');

    // Definindo o conteúdo da infowindow
    infowindow.setContent(newArrClusters);

    // Definiindo a posição (Que é o centro do cluster clicado)
    infowindow.setPosition(cluster.getCenter());

    // Abrindo a infowindow
    infowindow.open(map);
  }

}
